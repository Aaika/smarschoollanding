import Vue from 'vue'
import App from './App.vue'
import '@/assets/styles/styles.css'
import Mouseover from 'vue-mouseover';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import router from "@/Router/router";
library.add(faCoffee)

Vue.component('font-awesome-icon',Mouseover, FontAwesomeIcon)
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
