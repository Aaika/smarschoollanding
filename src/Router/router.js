import Vue from 'vue'
import Router from 'vue-router'
import MainPage from "@/components/MainPage/MainPage";
import MainOpportunities from "@/components/Opportunities/MainOpportunities";
import aboutProject from "@/components/about/aboutProject";
import registration from "@/components/registration /registration";
import signIn from "@/components/Login/sign-in";
import support from "@/components/support/support";
import GosOpportunity from "@/components/Opportunities/GosOpportunity";
import TeacherOpportunity from "@/components/Opportunities/TeacherOpportunity";
import ParentOpportunity from "@/components/Opportunities/ParentOpportunity";
import StudentOpportunity from "@/components/Opportunities/StudentOpportunity";
import Opportunities from "@/components/Opportunities/Opportunities";
Vue.use(Router);
let router= new Router({
    routes:[
        {
            path:'/',
            name :'home',
            component:MainPage,
            props: true
        },
        {
            path: '/opportunities',
            name: 'opportunities',
            component: MainOpportunities,
            props:true

        }
        ,
        {
            path: '/about',
            name: 'about',
            component: aboutProject,
            props:true

        }
        ,
        {
            path: '/support',
            name: 'support',
            component: support,
            props:true

        } ,
        {
            path: '/opportunity',
            name: 'opportunity',
            component: Opportunities,
            props:true

        }

        ,
        {
            path: '/registration',
            name: 'registration',
            component: registration,
            props:true

        }
        ,
        {
            path: '/signin',
            name: 'signIn',
            component: signIn,
            props:true

        }
        ,
        {
            path: '/teacheropportunity',
            name: 'teacheropportunity',
            component: TeacherOpportunity,
            props:true

        },


        {
            path: '/gosopportunity',
            name: 'gosopportunity',
            component: GosOpportunity,
            props:true

        },
        {
            path: '/parentopportunity',
            name: 'parentopportunity',
            component: ParentOpportunity,
            props:true

        },
        {
            path: '/studentopportunity',
            name: 'studentopportunity',
            component: StudentOpportunity,
            props:true

        }
    ]

})

export default router